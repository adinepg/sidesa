
<!-- Brand Logo -->
<a href="/" class="brand-link">
    <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Sidesa</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-2 pb-2 mb-2 d-flex">
    <nav class="">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon">
                        <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image" width="50">
                    </i>
                    <p>
                    Alexander Pierce
                    </p>
                    <i class="fas fa-angle-left right"></i>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/table" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div>


    <!-- SidebarSearch Form -->
    <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
            <button class="btn btn-sidebar">
                <i class="fas fa-search fa-fw"></i>
            </button>
            </div>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mb-5 pb-3 mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
            <li class="nav-item menu-open">
                <a href="/" class="nav-link active">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            
            <li class="nav-item">
                <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-user-friends"></i>
                    <p>Pengguna</p>
                </a>
            </li>
            
            <li class="nav-item">
                <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>Artikel Desa</p>
                </a>
            </li>

            <li class="nav-item">
                <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>Struktur Desa</p>
                </a>
            </li>

            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-book"></i>
                    <p>
                    APBDesa
                    <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/kegiatan" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Jenis Kegiatan</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/data-table" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Kategori Kegiatan</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/cast" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Rekap APBDesa</p>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-flag"></i>
                    <p>Pengaduan Masyarakat</p>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->