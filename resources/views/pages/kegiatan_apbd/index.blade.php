@extends('layouts.master')

@section('title')
    Halaman Data Kegiatan
@endsection

@push('styles')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@push('scripts')
    <script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>

    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

    <script>
        $(document).ready(function () {
        $("#tabelKegiatan").DataTable({
            deferRender: true,
        });
    });
    </script>
@endpush
    
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0">Halaman Data Kegiatan</h1>
                </div>
            </div><!-- /.row -->
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="/form-kegiatan" class="btn btn-primary">
                                    Add Kegiatan
                                </a>
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>

                        <div class="card-body">
                            <table id="tabelKegiatan" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Kegiatan</th>
                                        <th>Tahun Kegiatan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($kegiatan as $key=>$value)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$value->nama_kegiatan}}</td>
                                        <td>{{$value->tahun}}</td>
                                        <td>
                                            <button type="button" class="btn btn-info mb-2" data-toggle="modal" data-target="#modal-default{{$value->id}}">Detail</button>
                                            <a href="/kegiatan/{{$value->id}}/edit" class="btn btn-warning mb-2">Edit</a>
                                            <button type="button" class="btn btn-danger mb-2" data-toggle="modal" data-target="#modal-delete{{$value->id}}">Delete</button>
                                            {{-- <input type="submit" value="Delete" class="btn btn-danger mb-2">
                                            <form action="/kegiatan" method="post">
                                            </form> --}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
    
    {{-- MODAL EDIT --}}
    @foreach($kegiatan as $value)
    <div class="modal fade" id="modal-default{{$value->id}}">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Detail {{$value->nama_kegiatan}} {{$value->tahun}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4>Nama: {{$value->nama_kegiatan}}</h4>
                    <h4>Tahun: {{$value->tahun}}</h4>
                    <h4>Tgl Ditambahkan: {{$value->created_at}}</h4>
                    <h4>Terakhir diubah: {{$value->updated_at}}</h4>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    {{-- MODAL EDIT --}}
    
    {{-- MODAL DELETE --}}
    @foreach($kegiatan as $value)
    <div class="modal fade" id="modal-delete{{$value->id}}">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title">PERINGATAN!</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>Yakin akan menghapus data {{$value->nama_kegiatan}} {{$value->tahun}}? <br> Data yang telah dihapus tidak bisa di-restore.</h5>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <form action="/hapus-kegiatan/{{$value->id}}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                  </div>
            </div>
        </div>
    </div>
    @endforeach
    {{-- MODAL DELETE --}}


@endsection