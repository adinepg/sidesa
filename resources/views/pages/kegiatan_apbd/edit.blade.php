@extends('layouts.master')

@section('title')
    Halaman Edit Kegiatan
@endsection
    
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0">Halaman Edit Kegiatan</h1>
            </div>
        </div><!-- /.row -->
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="/cast" class="btn btn-warning">
                                    Back to List
                                </a>
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>

                        <div class="card-body">
                            <form action="/edit-kegiatan/{{$kegiatan->id}}" method="post">
                                @csrf
                                <div class="form-group">
                                  <label for="namekegiatan">Nama Kegiatan</label>
                                  <input type="text" class="form-control" id="namekegiatan" name="nama_kegiatan" value="{{$kegiatan->nama_kegiatan}}">
                                  @error('nama_kegiatan')
                                      <div class="alert alert-danger">
                                          {{ $message }}
                                      </div>
                                  @enderror
                                </div>
                                <div class="form-group">
                                  <label for="Tahunform">Tahun Kegiatan</label>
                                  <input type="number" class="form-control" id="Tahunform" name="tahun" value="{{$kegiatan->tahun}}">
                                  @error('tahun')
                                      <div class="alert alert-danger">
                                          {{ $message }}
                                      </div>
                                  @enderror
                                </div>
                                <button type="submit" class="btn btn-primary">Save Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection