@extends('layouts.master')

@section('title')
    Halaman Edit Data Cast
@endsection
    
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0">Halaman Edit Data Cast</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Table</a></li>
                <li class="breadcrumb-item active">Halaman Edit Data Cast</li>
            </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="/cast" class="btn btn-warning">
                                    Back to List
                                </a>
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>

                        <div class="card-body">
                            <form action="/cast/{{$cast->id}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                  <label for="nameform">Nama Lengkap</label>
                                  <input type="text" class="form-control" value="{{$cast->nama}}" id="nameform" name="nama">
                                </div>
                                <div class="form-group">
                                  <label for="umurform">Umur</label>
                                  <input type="number" class="form-control" value="{{$cast->umur}}" id="umurform" name="umur">
                                </div>
                                <div class="form-group">
                                  <label for="bioform">Bio</label>
                                  <textarea class="form-control" name="bio" id="bioform" cols="10" rows="3">{{$cast->bio}}</textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Update Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection