@extends('layouts.master')

@section('title')
    Halaman Data Cast
@endsection

@push('styles')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@push('scripts')
    <script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>

    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

    <script>
        $(document).ready(function () {
        $("#tabelCast").DataTable({
            deferRender: true,
        });
    });
    </script>
@endpush
    
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0">Halaman Data Cast</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Table</a></li>
                <li class="breadcrumb-item active">Halaman Data Cast</li>
            </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="/cast/create" class="btn btn-primary">
                                    Add Cast
                                </a>
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>

                        <div class="card-body">
                            <table id="tabelCast" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Umur</th>
                                        <th>Bio</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cast as $key=>$value)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$value->nama}}</td>
                                        <td>{{$value->umur}}</td>
                                        <td>{{$value->bio}}</td>
                                        <td>
                                            <form action="/cast/{{$value->id}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <a href="/cast/{{$value->id}}" class="btn btn-info mb-2">Detail</a>
                                                <a href="/cast/{{$value->id}}/edit" class="btn btn-warning mb-2">Edit</a>
                                                <input type="submit" value="Delete" class="btn btn-danger mb-2">
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Umur</th>
                                        <th>Bio</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection