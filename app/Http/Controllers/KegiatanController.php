<?php

namespace App\Http\Controllers;

use App\Models\Kegiatan;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class KegiatanController extends Controller
{
    public function index(){
        $kegiatan=Kegiatan::all();
        return view('pages.kegiatan_apbd.index', compact('kegiatan'));
    }
    
    public function create(){
        return view('pages.kegiatan_apbd.form');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_kegiatan' => 'required',
            'tahun' => 'required|numeric',
        ]);

        Kegiatan::create([
            'nama_kegiatan' => $request->nama_kegiatan,
    		'tahun' => $request->tahun
        ]);

        return redirect('/kegiatan');
    }

    public function show($id)
    {
        $kegiatan = Kegiatan::find($id);
        return view('pages.kegiatan_apbd.detail', compact('kegiatan'));
    }
    
    public function edit($id)
    {
        $kegiatan = Kegiatan::find($id);
        return view('pages.kegiatan_apbd.edit', compact('kegiatan'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_kegiatan' => 'required',
            'tahun' => 'required|numeric',
        ]);
        
        $kegiatan = Kegiatan::find($id);
        $kegiatan->nama_kegiatan = $request->nama_kegiatan;
        $kegiatan->tahun = $request->tahun;
        $kegiatan->update();
        // $kegiatan = Kegiatan::findOrFail($id);
        // $kegiatan->update($request->all());

        return redirect('/kegiatan');
    }

    public function destroy($id)
    {
        $kegiatan = Kegiatan::find($id);
        $kegiatan->delete();
        return redirect('/kegiatan');
    }
}
