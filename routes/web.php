<?php

use App\Http\Controllers\KegiatanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
});

Route::get('/kegiatan', [KegiatanController::class, 'index']);
Route::get('/form-kegiatan', [KegiatanController::class, 'create']);
Route::post('/add-kegiatan', [KegiatanController::class, 'store']);
Route::get('/kegiatan/{kegiatan_id}', [KegiatanController::class, 'show']);
Route::get('/kegiatan/{kegiatan_id}/edit', [KegiatanController::class, 'edit']);
Route::post('/edit-kegiatan/{kegiatan_id}', [KegiatanController::class, 'update']);
Route::post('/hapus-kegiatan/{kegiatan_id}', [KegiatanController::class, 'destroy']);